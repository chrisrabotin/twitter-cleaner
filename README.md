# twitter-cleaner

A very simple project to clean up your Twitter activity older than a given date (currently set to six months).

## Environment variables
 - CAK: consumer API key
 - CSK: consumer secret key
 - AT: access token
 - ATS: access token secret
 - TUSER: Twitter user


## Example run
```
(twitter-cleaner-MOmuh_TM)  ✘ chris@olympus  ~/Workspace/twitter-cleaner   issue-1  python clean.py --yes
Connecting to the API...
Connected.
Fetching tweets 1 to 200
Fetching tweets 201 to 400
Fetching tweets 401 to 600
Fetching tweets 601 to 800
Fetching tweets 801 to 1000
Fetching tweets 1001 to 1200
Fetching tweets 1201 to 1400
Fetching tweets 1401 to 1600
Fetching tweets 1601 to 1800
Fetching tweets 1801 to 2000
Fetching tweets 2001 to 2200
Fetching tweets 2201 to 2400
Fetching tweets 2401 to 2600
Fetching tweets 2601 to 2800
Fetching tweets 2801 to 3000
Fetching tweets 3001 to 3200
Will now delete 2578 tweets
Deleted 1 tweets
Could not delete status ID 992071751126302720: [{'code': 144, 'message': 'No status found with that ID.'}]
Deleted 101 tweets
Deleted 201 tweets
Could not delete status ID 959151905120313344: [{'code': 144, 'message': 'No status found with that ID.'}]
Deleted 301 tweets
Deleted 401 tweets
Could not delete status ID 935918690083713024: [{'code': 144, 'message': 'No status found with that ID.'}]
```
