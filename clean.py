import argparse
import os
import pdb
from datetime import datetime, timedelta
from time import sleep

import twitter

if __name__ != '__main__':
    exit()

parser = argparse.ArgumentParser(description='Delete twitter history older than six months')
parser.add_argument('--yes', action='store_true', help='Pass the flag to confirm the deletion')
args = parser.parse_args()

if not args.yes:
    print('[WARNING] Dry run only!')

for (envk, msg) in [('CAK', 'consumer access key'), ('CSK', 'consumer secret key'),
                    ('AT', 'access token'), ('ATS', 'access token secret'), ('TUSER', 'username')]:
    try:
        locals()[envk.lower()] = os.environ[envk]
    except KeyError:
        raise KeyError('missing environment for {}, the {}'.format(envk, msg))

print('Connecting to the API...')
api = twitter.Api(consumer_key=cak, consumer_secret=csk,
                  access_token_key=at, access_token_secret=ats,
                  sleep_on_rate_limit=True)
print('Connected.')

start_date = datetime.now() - timedelta(days=30.5 * 6)
stati = []
start_id = None
fetch = 200
for page in range(1000):  # Only get up to 200,000 tweets
    print('Fetching tweets {} to {}'.format(page * fetch + 1, (page + 1) * fetch))
    tweets = api.GetUserTimeline(screen_name=tuser, max_id=start_id, count=fetch, trim_user=True)
    next_start_id = min(tweets, key=lambda t: t.id).id
    # Keep all tweets which were are older than the desired time
    stati += [tweet for tweet in tweets if tweet.created_at_in_seconds < start_date.timestamp()]
    if not len(tweets) or next_start_id == start_id:
        # We have reached the end of all of the stati
        break
    start_id = next_start_id

if args.yes:
    print('Will now delete {} tweets'.format(len(stati)))
    for cnt, status in enumerate(stati):
        try:
            api.DestroyStatus(status.id, trim_user=True)
        except twitter.error.TwitterError as e:
            print('Could not delete status ID {}: {}'.format(status.id, e.message))
        if cnt % 100 == 0:
            print('Deleted {} tweets'.format(cnt + 1))
    print('Deleted {} tweets'.format(cnt + 1))
else:
    print('Dry run enabled! Nothing was deleted.')
